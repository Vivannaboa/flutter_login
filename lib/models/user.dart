class User {
  String _username;
  String _password;
  String _token;

  User(this._username, this._password);

  User.map(dynamic obj) {
    this._username = obj["usuario"];
    this._password = obj["senha"];
    this._token = obj["token"];
  }

  String get username => _username;
  String get password => _password;
  String get token => _token;

  Map<String, dynamic> toMap() {
    var map = new Map<String, dynamic>();
    map["usuario"] = _username;
    map["senha"] = _password;

    return map;
  }
}