import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_login/auth.dart';
import 'package:flutter_login/pages/login/login_screen_presenter.dart';

class LoginScreen extends StatefulWidget {
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen>
    implements LoginScreenContract, AuthStateListener {
//Variaves
  BuildContext _ctx;
  bool _isLoading = false;
  bool _obscureText = true;
  final formKey = new GlobalKey<FormState>();
  final scaffoldKey = new GlobalKey<ScaffoldState>();
  String _password, _username;
  LoginScreenPresenter _presenter;

  _LoginScreenState() {
    _presenter = new LoginScreenPresenter(this);
    var authStateProvider = new AuthStateProvider();
    authStateProvider.subscribe(this);
  }

  void _submit() {
    final form = formKey.currentState;

    if (form.validate()) {
      setState(() => _isLoading = true);
      form.save();
      _presenter.doLogin(_username, _password);
    }
  }

  void _showSnackBar(String text) {
    scaffoldKey.currentState
        .showSnackBar(new SnackBar(content: new Text(text)));
  }

  void _toggle() {
    setState(() {
      _obscureText = !_obscureText;
    });
  }

  @override
  onAuthStateChanged(AuthState state) {
    if (state == AuthState.LOGGED_IN)
      Navigator.of(_ctx).pushReplacementNamed("/home");
  }

  @override
  Widget build(BuildContext context) {
    _ctx = context;
    var btnEntrar = new Padding(
      padding: const EdgeInsets.all(12.0),
      child: new RaisedButton(
        child: new Text("Entrar"),
        onPressed: _submit,
        color: Colors.primaries[0],
      ),
    );

    var loginForm = Padding(
      padding: const EdgeInsets.all(18.0),
      child: new Column(
        children: <Widget>[
          new Form(
            key: formKey,
            child: new Column(
              children: <Widget>[
                new Padding(
                  padding: const EdgeInsets.all(2.0),
                  child: new TextFormField(
                    decoration: const InputDecoration(
                      icon: const Icon(Icons.email),
                      hintText: 'Seu endereço de e-mail',
                      labelText: 'E-mail',
                    ),
                    keyboardType: TextInputType.emailAddress,
                    onSaved: (val) => _username = val,
                    validator: (val) {
                      if (!isEmail(val)) {
                        return 'E-mail inválido!';
                      }
                      if (val.isEmpty) {
                        return 'Informe um e-mail';
                      }
                    },
                  ),
                ),
                new Padding(
                  padding: const EdgeInsets.all(2.0),
                  child: new Column(children: <Widget>[
                    new TextFormField(
                      decoration: const InputDecoration(
                          labelText: 'Password',
                          icon: const Padding(
                              padding: const EdgeInsets.only(top: 15.0),
                              child: const Icon(Icons.lock))),
                      validator: (val) =>
                          val.length < 6 ? 'A senha edeve conter no minimo 6 caracteres' : null,
                      onSaved: (val) => _password = val,
                      obscureText: _obscureText,
                    ),
                    new FlatButton(
                        onPressed: _toggle,
                        child: new Text(_obscureText ? "Show" : "Hide"))
                  ]),
                ),                
              ],
            ),
          ),
          _isLoading ? btnEntrar : btnEntrar
        ],
        crossAxisAlignment: CrossAxisAlignment.stretch,
      ),
    );

     return new Scaffold(
      resizeToAvoidBottomPadding: true,
      appBar: null,
      key: scaffoldKey,
      body: new ListView(
        children: <Widget>[
          new Image.asset(
            "imagens/logo.png",
            fit: BoxFit.contain,
          ),
          new Container(
            child: loginForm,
          ),
        ],
      ),
    );
  }

  @override
  void onLoginError(String errorTxt) {
    _showSnackBar(errorTxt);
    setState(() => _isLoading = false);
  }

  @override
  void onLoginSuccess() {
    setState(() => _isLoading = false);
    var authStateProvider = new AuthStateProvider();
    authStateProvider.notify(AuthState.LOGGED_IN);
  }
}
