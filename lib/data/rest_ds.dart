// import 'dart:async';
// import 'dart:convert';

// import 'package:flutter_login/models/user.dart';
// import 'package:flutter_login/utils/network_util.dart';

// class RestDatasource {
//   NetworkUtil _netUtil = new NetworkUtil();
//   static final BASE_URL = "http://www.wsapp.moveisherval.com.br/api/";
//   static final LOGIN_URL = BASE_URL + "Login";
//   static final _API_KEY = "";

//   Future<User> login(String username, String password) async {
//     var body = json.encode({
//       "email": username,
//       "senha": password
//     });
//     return _netUtil.post(LOGIN_URL, body:body , headers:{
//       "Content-type": 'application/json',
//       "Accept": 'application/json'
//       }
//     ).then((dynamic res) {

//       if(!res["success"]){
//         throw new Exception('No joke for you!');
//       }
//       //  throw new StateError(res["data"]["erro"][0]);
//       print(res["data"]["token"]);
//       return new User.map(res["data"]);
//     }
//     );
//   }
// }

import 'dart:async';
import 'dart:convert';

import 'package:flutter_login/constant.dart';
import 'package:flutter_login/data/database_helper.dart';
import 'package:flutter_login/models/auth.dart';
//import 'package:flutter_login/models/message.dart';
import 'package:flutter_login/utils/network_util.dart';

class RestDatasource {
  NetworkUtil _netUtil = new NetworkUtil();

  Future<Auth> login(String email, String password) {
    var loginUrl = "$backendUrl/Login";
    return getBasicHeaders().then((dynamic headers) {
      return _netUtil.post(loginUrl,body: json.encode({"email": email, "senha": password}),headers: headers)
      .then((dynamic res) {
        var body = JSON.decode(res.body);
      
        print(body.toString());
        
        
        if (!body["success"]) {
          var erros = body["data"];
          var retErro;
          erros.forEach((k,v) => retErro = '${k}: ${v}');
          throw new Exception(retErro.replaceAll('[', '').replaceAll(']',''));
        }
        return new Auth.map(res);
      });
    });
  }

  // Future<ListMessage> getMessages(int page) {
  //   var messageUrl = "$backendUrl/api/messages?page=$page";
  //   return getHeaders().then((dynamic headers) {
  //     return _netUtil.get(messageUrl, headers).then((dynamic res) {
  //       var body = JSON.decode(res.body);

  //       print(body.toString());
  //       if (body["error"] != null) throw new Exception(body["error_msg"]);

  //       return new ListMessage.map(body);
  //     });
  //   });
  // }

  Future logout() {
    var messageUrl = "$backendUrl/api/auth/sign_out";
    return getHeaders().then((dynamic headers) {
      return _netUtil.delete(messageUrl, headers: headers).then((dynamic res) {
        var body = JSON.decode(res.body);

        print(body.toString());
        
        if (body["error"] != null) throw new Exception(body["error_msg"]);

        return;
      });
    });
  }

  Future<dynamic> readAll() {
    var userRoomUrl = "$backendUrl/api/user_room";
    return getHeaders().then((dynamic headers) {
      return _netUtil
          .put(userRoomUrl, body: {}, headers: headers)
          .then((dynamic res) {
        try {
          var body = JSON.decode(res.body);
          print(body.toString());
          if (body["error"] != null) throw new Exception(body["error_msg"]);
          return body;
        } catch (_) {
          return {};
        }
      });
    });
  }

  Future<dynamic> getHeaders() async {
    var db = new DatabaseHelper();
    var auth = await db.getAuth();
    return {
      "UID": auth.uid,
      "ACCESS_TOKEN": auth.accessToken,
      "DEVICE_TOKEN": auth.deviceToken,
      "CLIENT": auth.clientId
    };
  }

  Future<dynamic> getBasicHeaders() async {
    return {"Content-type": 'application/json', "Accept": 'application/json'};
  }
}
