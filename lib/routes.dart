import 'package:flutter/material.dart';
import 'package:flutter_login/pages/home.dart';
import 'package:flutter_login/pages/login/login.dart';


final routes = {
  '/login': (BuildContext context) => new LoginScreen(),
  '/home':  (BuildContext context) => new HomeScreen(),
  '/' :     (BuildContext context) => new LoginScreen(),
};